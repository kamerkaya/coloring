About Bipartite Graph Partial Coloring Results:

VV: Vertex based coloring, vertex based conflict detection 
VV64: with dynamic chunksize = 64
VV64NC: with no critical region
VNFULL: Vertex based coloring, net based conflict detection
VN1: Vertex based coloring, net based conflict detection for one iteration (rest is vertex based)
VN2: Vertex based coloring, net based conflict detection for two iterations(rest is vertex based)
N1N2: Net based coloring for one iteration, net based conflict detecion for two iterations(rest is vertex based)
N2N2: Net based coloring for two iterations, net based conflict detection for two iterations(rest is vertex based)
VN2U: VN2 without using balanced coloring. (Unbalanced)
VN2B1: Using balancing algorithm 1
VN2B2: Using balancing algorithm 2
N1N2U: N1N2 without using balanced coloring. (Unbalanced) 
N1N2B1: Using balancing algorithm 1
N1N2B2: Using balancing algorithm 2